const INITIAL_STATE = {
  playing: false,
  initialized: false,
  masterGainValue: 50,
  synths: [],
  pinkNoise: []
}

export const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "LOAD_SETTINGS": {
      return {...state, settings: action.payload}
    }
    case "TOGGLE_PLAY": {
      return {...state, playing: !state.playing, initialized: true}
    }
    case "SET_PINK_NOISE": {
      return {...state, pinkNoise: [...state.pinkNoise, action.payload]}
    }
    case "SET_SINE_OSC": {
      return {...state, sineOsc: action.payload}
    }
    case "SET_AUDIO_CONTEXT": {
      return {...state, audioCtx: action.payload}
    }
    case "SET_MASTER_GAIN": {
      return {...state, masterGain: action.payload}
    }
    case "SET_SCHEDULER": {
      return {...state, scheduler: action.payload}
    }
    case "SET_MASTER_GAIN_VALUE": {
      return {...state, masterGainValue: action.payload}
    }
    case "SET_SYNTH_NODE": {
      return {...state, synths: 
        [...state.synths, action.payload]}
    }
    case "SET_SYNTH_GAIN": {
      return {...state, 
        synths: state.synths.map( (synth, index) => {
          if (index === action.payload.index) {
            return {...synth, gainValue: action.payload.value}
          } else {
            return synth
          }
        })
      }
    }
    default: 
      return state
  }
}

export default reducer