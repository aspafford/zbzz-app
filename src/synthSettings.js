const settings = {

  pinkNoise: [
    {
      modFq: 0.1,
      flatGain: 0.15,
      maxGain: 0.04,
      filterFq: 15900,
      filterQ: 0.48,
      pan: -0.66
    },
    {
      modFq: 0.17,
      flatGain: 0.15,
      maxGain: 0.04,
      filterFq: 14900,
      filterQ: 0.42,
      pan: 0.66
    },
    {
      modFq: 0.08,
      flatGain: 0.2,
      maxGain: 0.06,
      filterFq: 190,
      filterQ: 0.28,
      pan: -0.33
    },
    {
      modFq: 0.06,
      flatGain: 0.2,
      maxGain: 0.06,
      filterFq: 100,
      filterQ: 0.22,
      pan: 0.33
    }
  ],
  clicks: [
    {
      density: 0.3,
      gain: 0.4,
      pan: 0.9
    },
    {
      density: 0.9,
      gain: 0.25,
      pan: 0.6
    },
    {
      density: 5,
      gain: 0.15,
      pan: 0.3
    },
  ],
  clickStream: [
    {
      numClicks: 15,
      bufferLength: 5,
      gain: 0.10
    },
    {
      numClicks: 60,
      bufferLength: 6,
      gain: 0.02
    },
  ],
  pulseWaveGroups: [
  {
    getNumSynths: () => Math.ceil(Math.random() * 4) + 1,
    pitchMul: 0.6,
    delay: 0
  },
  {
    getNumSynths: () => Math.ceil(Math.random() * 2) + 1,
    pitchMul: 1.2,
    delay: 0.4
  },
  {
    getNumSynths: () => Math.ceil(Math.random() * 2) + 1,
    pitchMul: 2.4,
    delay: 0.8
  },
  // {
  //   getNumSynths: () => 2,
  //   pitchMul: 0.5,
  //   gain: 0.55,
  //   delay: 0.5
  // }

  ]
}

module.exports = settings
