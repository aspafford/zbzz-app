import React from 'react'

export const Scope = props =>
  <div className="canvas-container">
    <canvas id="canvasLeft"></canvas>
    <canvas id="canvasRight"></canvas>
  </div>
