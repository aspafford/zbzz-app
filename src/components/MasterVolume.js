import React from 'react';
import { connect } from 'react-redux'
import styled from 'styled-components'
import Typography from '@material-ui/core/Typography';
// import Slider from '@material-ui/lab/Slider';
import { Range } from 'rc-slider'
// actions
import { updateMasterGain } from '../actions/masterGain'

const mapDispatchProperties = 
  dispatch => ({
    handleChange: (event) => {
      dispatch(updateMasterGain(event[0]))
    }
  })

const mapStateToProps = state => ({
  volume: state.masterGainValue
})
const Wrapper = styled.div`
  padding: 10px;
  color: white;
`;
const SliderWrapper = styled.div`
  background-color: white;
  border: 1px solid;
  border-radius: 2px;
  padding: 10px;
  width: 150px;
  margin: 0 auto;
`;

const VolumeSlider = props => 
  <Wrapper>
    <Typography style={{color: 'white'}} id="label">Master volume</Typography>
    <SliderWrapper>
      <Range 
        value={[props.volume]} 
        aria-labelledby="label" 
        onChange={props.handleChange} 
      />
    </SliderWrapper>
  </Wrapper>

export default connect(mapStateToProps, mapDispatchProperties)(VolumeSlider)
