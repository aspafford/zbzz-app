import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
//actions
import { togglePlay } from '../actions/togglePlay'
// import Button from '@material-ui/core/Button';

const Wrapper = styled.div`
  padding: 10px;
`;
const Button = styled.div`
background-color: white;
  display: inline-block;
  width: 150px;
  padding: 20px 10px;
  border-radius: 2px;
  cursor: pointer;
  &:hover {
    background-color: #3E54AF;
    color: white;
  }
`;

const mapDispatchProperties = dispatch => ({
  togglePlay: () => {
    dispatch(togglePlay());
  }
})

const mapStateToProps = state => ({
  playing: state.playing
})

const View = props =>
  <Wrapper className="play-container">
    <Button 
      onClick={props.togglePlay}
    >
      {props.playing ? 'pause' : 'play'}
    </Button>
  </Wrapper>

export const PlayButton = connect(mapStateToProps, mapDispatchProperties)(View)