import React from 'react';
import { connect } from 'react-redux'
import styled from 'styled-components'
import Typography from '@material-ui/core/Typography';
// import Slider from '@material-ui/lab/Slider'; // sux on mobile
import { Range } from 'rc-slider'
// actions
import { updateSynthGain } from '../actions/updateSynth'

import 'rc-slider/assets/index.css';

const mapDispatchProperties = 
  dispatch => ({
    // handleChange: (index, synth, event, value) => {
      // dispatch(updateSynthGain(index, synth, event, value))
    handleChange: (index, synth, event) => {
      // console.log('event', event)
      dispatch(updateSynthGain(index, synth, event))
    }
  })

const mapStateToProps = state => ({
  synths: state.synths
})

const Wrapper = styled.div`
border: 1px solid white;
  border-radius: 2px;
  margin: 0 10px;
  padding: 10px;
  background-color: #222;
`;
const SliderWrapper = styled.div`
  padding: 10px;
  width: 150px;
  margin: 0 auto;
  color: white;
`;

const VolumeSlider = props => {
  return (
    <SliderWrapper>
      <Typography style={{color: "white"}} id="label">{props.synth.id}</Typography>
      <Range 
        value={[(props.synth.gainValue >= 0) ? props.synth.gainValue : 50]} 
        aria-labelledby="label" 
        onChange={props.handleChange.bind(this, props.index, props.synth)} 
      />
    </SliderWrapper>
  )
}

const SynthControls = props => {
  if (!props.synths) return <div/>
  return (
    <Wrapper>
    {props.synths.map( (synth, index) => 
      <VolumeSlider 
        key={index}
        index={index}
        synth={synth}
        mul={synth.mul}
        noiseGain={props.noiseGain}
        handleChange={props.handleChange} 
      />
    )}
    </Wrapper>
  )
}

export default connect(mapStateToProps, mapDispatchProperties)(SynthControls)