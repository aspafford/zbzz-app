export const getPanArray = numSynths => {

  const arr = []
  const increment = 1 / (numSynths + 1)

  for (let i = 0; i < numSynths; i++) {
    // scale to -1 to +1 range
    let val = increment * (i + 1)
    val *= 2
    val -= 1

    arr.push(val)
  }

  return arr
}