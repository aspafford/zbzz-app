import _ from 'underscore'

import ClickStream from '../synth/ClickStream'
// actions
import { setSynthNode } from './initSynth'

export const startClickStream = () => {
  return (dispatch, getState) => {

    const s = getState()

    const gainNode = _.find(s.synths, {id: 'clicks'})

    const filter = s.audioCtx.createBiquadFilter()
    filter.type = "bandpass"
    filter.frequency.value = 900
    filter.Q.value = 0.2

    filter.connect(gainNode.gain)
    gainNode.gain.connect(s.masterGain)

    s.settings.clickStream.forEach(synth => {
      let clickStream = new ClickStream(s.audioCtx, filter, synth)
      clickStream.start(
        s.audioCtx.currentTime, 
        synth
      )
    })
  }
}
