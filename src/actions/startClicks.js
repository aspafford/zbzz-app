import Reverb from 'soundbank-reverb'
import Clicks from '../synth/Clicks'
import _ from 'underscore'
// actions
import { setSynthNode } from './initSynth'

export const startClicks = () => {
  return (dispatch, getState) => {
    const s = getState()

    const gainNode = _.find(s.synths, {id: 'clicks'})

    // click synth
    s.settings.clicks.forEach((synth, index) => {

      // gain
      const gain = s.audioCtx.createGain()
      gain.gain.value = synth.gain
      // pan (L/R channels)
      const pannerL = s.audioCtx.createStereoPanner()
      pannerL.pan.value = synth.pan * -1
      const pannerR = s.audioCtx.createStereoPanner()
      pannerR.pan.value = synth.pan
      const panArray = [pannerL, pannerR]
      // bandpass filter
      const filter = s.audioCtx.createBiquadFilter()
      filter.type = "bandpass"
      filter.frequency.value = 350
      filter.Q.value = 0.7
      // reverb
      const reverb = new Reverb(s.audioCtx)
      reverb.time = 0.25
      reverb.wet.value = 0.9
      reverb.dry.value = 0.7
      // reverb.filterType = 'lowpass'
      // reverb.cutoff.value = 4000

      // connections
      pannerL.connect(filter)
      // pannerL.connect(reverb)
      pannerR.connect(filter)
      // pannerR.connect(reverb)
      // reverb.connect(filter)
      filter.connect(gain)
      gain.connect(gainNode.gain)
      gainNode.gain.connect(s.masterGain)

      const loop = () => {
        window.loopTimer = setTimeout( () => {
          // randomly pick l/r panning
          const randPanIndex = Math.floor(Math.random() * 2)

          const clicks = new Clicks(s.audioCtx, panArray[randPanIndex])
          clicks.start(
            s.audioCtx.currentTime,
            synth
          )
          if (s.audioCtx.state !== "suspended") {
            loop()
          }
        }, (1000 / synth.density))
      }
      loop()
    })
  }
}