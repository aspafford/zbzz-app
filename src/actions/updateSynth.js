export const setSynthGain = (index, value) => ({
  type: 'SET_SYNTH_GAIN',
  payload: {index: index, value: value}
})

export const updateSynthGain = (index, synth, event) => {
  const value = event[0]
  return (dispatch, getState) => {
    const s = getState()
    const gainValue = value * 0.01
    s.synths[index].gain.gain.setTargetAtTime(
      gainValue, 
      s.audioCtx.currentTime + 0.1, 
      0.01
    )
    // update ui state
    dispatch(setSynthGain(index, value))
  }
}