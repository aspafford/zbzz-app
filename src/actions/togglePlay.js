import _ from 'underscore'
// play synths
import { startPinkNoise } from './startPinkNoise'
import { startClicks } from './startClicks'
import { startClickStream } from './startClickStream'
import { startPulseWave } from './startPulseWave'
// import { startSine } from './startSine'
// actions
import { setSynthNode } from './initSynth'

const togglePlayReducer = () => ({
  type: 'TOGGLE_PLAY'
})

export const togglePlay = () => {
  return (dispatch, getState) => {
    const s = getState()


    if (s.playing) {
      s.audioCtx.suspend()
      // clear clicks loop
      clearTimeout(window.loopTimer)
    } else {
      s.audioCtx.resume()
      // restart timeout loops
      // dispatch(startClicks()) // 14-18 cpu
    }

    // WARNING FIREFOX BUG -- needs to happen AFTER resume
    // just fire once  first time
    // !s.initialized && dispatch(startSine()) // 19-20 cpu

    !s.initialized && dispatch(startPulseWave()) // 19-20 cpu
    !s.initialized && dispatch(startPinkNoise())  // 4-5 cpu

    // first time: add gain channel to ui to control both 'click' synths
    const found = _.find(s.synths, {id: 'clicks'})
    if (!found) {
      const clickGain = s.audioCtx.createGain()
      clickGain.connect(s.masterGain)
      clickGain.gain.value = 0.5
      // add to state
      const synthOptions = {
        id: 'clicks',
        gain: clickGain,
        gainValue: 50
      }
      dispatch(setSynthNode(synthOptions))
    }

    !s.initialized && dispatch(startClickStream()) // 2-3 cpu


    // 2017 mbp 2.3GHz/8GB
    // total cpu 30-35

    dispatch(togglePlayReducer())
  }
}