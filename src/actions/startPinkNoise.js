import PinkNoise from '../synth/PinkNoise'
// actions
import { setSynthNode } from './initSynth'

export const startPinkNoise = () => {
  return (dispatch, getState) => {
    const s = getState()
    // top level gain
    const noiseGain = s.audioCtx.createGain()
    noiseGain.gain.value = 0.15
    noiseGain.connect(s.masterGain)

    // add to state
    const synthOptions = {
      id: 'noise',
      gain: noiseGain,
      gainValue: 15
    }

    dispatch(setSynthNode(synthOptions))

    // pink noise synth
    s.settings.pinkNoise.forEach(synth => {
      let pinkNoise = new PinkNoise(s.audioCtx, noiseGain)
      // dispatch(setPinkNoise(pinkNoise))
      pinkNoise.start(
        s.audioCtx.currentTime, 
        synth
      )
    })
  }
}