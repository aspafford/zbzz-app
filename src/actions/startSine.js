// import Sine from '../synth/Sine'
import PulseWave from '../synth/PulseWave'
import { Envelope } from '../synth/Envelope'
import { getRandomNote } from './helpers/getRandomNote'

const scale = [
 261.63, // middle C
293.66, //D
329.63, //E
  349.23, //F
    392, //G
    440, //A
    493.88, //B
    523.25, //C5
    587.33, //D
    659.25, // E
    698.46, //F
    783.99, // G
    880, // A
]

const scales2 = [
  // [1],
  [1, 2, 3, 5, 8],
  [0, 3, 4, 5, 8],
  [2, 4, 6, 10],
  [1, 4, 6, 7, 9],
]

const synthLength =  0.5 // seconds
const beats = 100
// const synthOverlap =  1.5 // seconds

const startScheduler = (s, cb) => { 
  const playback = [] 
  for (let i = 0; i < beats; i++) { 
    let scaleIndex = i % 4 // 4 bars 
    s.settings.pulseWaveGroups.forEach(group => { 
      const numSynths = group.getNumSynths() 
      for (let j = 0; j < numSynths; j++) { 
        let note = getRandomNote(scales2[scaleIndex]) 
        playback.push({ 
          freq: scale[note] * group.pitchMul, 
          position: i * synthLength, 
          length: synthLength,
          numSynths: numSynths,
        }) 
      } 
    }) 
  } 
  s.scheduler.on('data', function(schedule) { 
    // schedule: from, to, time, beatDuration 
    playback.forEach(function(note){ 
      if (note.position >= schedule.from && note.position < schedule.to){ 
        var delta = note.position - schedule.from 
        var time = schedule.time + delta 
        var duration = note.length * schedule.beatDuration 
        cb(s, time, duration, note) 
      } 
    }) 
  }) 
 
  s.scheduler.setTempo(60) 
 
  setTimeout(function(){ 
    s.scheduler.start() 
  }, 500) 

}

const play = (s, at, length, note) => {
  let sine = new PulseWave(s.audioCtx)
  sine.osc.frequency.setValueAtTime(note.freq, at)
  let gain = s.audioCtx.createGain()
  gain.gain.value = 0

  let v = note.numSynths > 9 ? 0.01 / note.numSynths : 0.01

  console.log('v:', v)

  sine.osc.connect(gain)
  gain.connect(s.masterGain)

  sine.osc.onended = function(event) {
    sine.osc.disconnect(gain)
    gain.disconnect(s.masterGain)
  }

  sine.osc.start(at)
  // do envelope this way -- Enveolope module was causing noise on pixel and iphone
  gain.gain.setValueAtTime(0, at)
  gain.gain.linearRampToValueAtTime(0.0001, at + length / 4)
  gain.gain.exponentialRampToValueAtTime(v, at + length / 2)
  gain.gain.linearRampToValueAtTime(0.0000001, at + length)
  // this.input.gain.linearRampToValueAtTime(this.sustainVol, this.time + this.sustain)
  // this.input.gain.linearRampToValueAtTime(this.min, this.time + this.decay)
  sine.osc.stop(at + length)
}

export const startSine = () => {
  return (dispatch, getState) => {
    const s = getState()
    startScheduler(s, play) 
  }
}