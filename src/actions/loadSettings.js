// reducers
export const loadSettings = settings => ({
  type: 'LOAD_SETTINGS',
  payload: settings
})