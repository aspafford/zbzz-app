import bopper from 'bopper'
import { setMasterGain } from './masterGain'

import { initScope } from './scope.js'

// reducers
const setAudioContext = payload => ({
  type: 'SET_AUDIO_CONTEXT',
  payload: payload
})
const setScheduler = payload => ({
  type: 'SET_SCHEDULER',
  payload: payload
})
export const setSynthNode = payload => ({
  type: 'SET_SYNTH_NODE',
  payload: payload
})


export const initSynth = () => {
  return (dispatch, getState) => {
    // set audio context
    // to-do: catch false condition
    const audioCtx = new (window.AudioContext || window.webkitAudioContext || false)();
    audioCtx.suspend()
    dispatch(setAudioContext(audioCtx))

    // set master gain
    const gain = audioCtx.createGain()
    gain.gain.value = 0.5
    gain.connect(audioCtx.destination)
    dispatch(setMasterGain(gain))
    // scope output
    const splitter = audioCtx.createChannelSplitter(2);
    const scopeLeft = initScope(audioCtx, 'canvasLeft')
    const scopeRight = initScope(audioCtx, 'canvasRight')
    gain.connect(splitter)
    splitter.connect(scopeLeft, 0)
    splitter.connect(scopeRight, 1)

    // bopper
    const scheduler = bopper(audioCtx)
    dispatch(setScheduler(scheduler))
    // save a reference on the window to avoid garbage collection
    window.scheduler = scheduler
  }
}