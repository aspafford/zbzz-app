const blue = "#35A7FF";
const red = "#FF5964";

export const initScope = (audioCtx, canvasId) => {

  const canvas = document.getElementById(canvasId);
  const analyser = audioCtx.createAnalyser();
  // analyser.fftSize = 2048;
  // analyser.fftSize = 1024;
  // analyser.fftSize = 512;
  analyser.fftSize = 256;

  var bufferLength = analyser.frequencyBinCount;

  const waveform = new Float32Array(bufferLength)

  ;(function updateWaveform() {
    requestAnimationFrame(updateWaveform)
    if (analyser.getFloatTimeDomainData) {
      analyser.getFloatTimeDomainData(waveform)
    }
  })()

  canvas.width = waveform.length

  console.log('waveform.length', waveform.length)

  var canvasCtx = canvas.getContext('2d');
  canvasCtx.lineWidth = 2;
  canvasCtx.strokeStyle = "#fff";

  ;(function drawOscilloscope() {

    requestAnimationFrame(drawOscilloscope)
    
    canvasCtx.clearRect(0, 0, canvas.width, canvas.height)
    canvasCtx.beginPath()

    // peak detection
    let max = 0
    // for (let i = 0; i < waveform.length; i++) {
    //   if (Math.abs(waveform[i]) > max) {
    //     max = Math.abs(waveform[i])
    //   }
    // }
    // if (max > 0.1) {
      for (let i = 0; i < waveform.length; i++) {
        const x = i
        const y = (0.5 + waveform[i] / 2) * canvas.height;
        if (i == 0) {
          canvasCtx.moveTo(x, y)
        } else {
            canvasCtx.lineTo(x, y)
        }
      }
    // }
    canvasCtx.stroke()
  })()

  return analyser
}
