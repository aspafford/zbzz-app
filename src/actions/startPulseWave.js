import Reverb from 'soundbank-reverb'
// import Sine from '../synth/Sine'
import PulseWave from '../synth/PulseWave'
import TripleLowpass from '../synth/TripleLowpass'
import StereoPannerNode from 'stereo-panner-node'
import { getRandomNote } from './helpers/getRandomNote'
import { getPanArray } from './helpers/getPanArray'
import { pulseWaveVariations } from './variations/pulseWaveVariations'
// actions
import { setSynthNode } from './initSynth'

StereoPannerNode.polyfill()

const scale = [
  261.63, // middle C
  293.66, //D
  329.63, //E
  349.23, //F
  392, //G
  440, //A
  493.88, //B
  523.25, //C5
  587.33, //D
  659.25, // E
  698.46, //F
  783.99, // G
  880, // A
]

const scales2 = [
  // [1],
  [1, 2, 3, 5, 8],
  [0, 3, 4, 5, 8],
  [2, 4, 6, 10],
  [1, 4, 6, 7, 9],
]
const scales3 = [
  // [1],
  [0,1,2,4,7],
  [6,2,3,4,7],
  [1,3,5,9],
  [0,3,5,6,8]
]

// const synthLength =  5 // seconds
const beats = 101
let prevLength = false
// const overlap = synthLength / 3
// const synthOverlap =  1.5 // seconds

const startScheduler = (s, out, cb) => { 
  const playback = [] 
  let position = 0

  let scales
  for (let i = 0; i < beats; i++) { 

    let scaleIndex = i % 4 // 4 bars 

    // every 4 bars randmoly chose mode
    if (scaleIndex === 0) {
      scales = Math.random() > 0.5 ? scales2 : scales3
    }

    // duration of the tone -- with randomization
    let synthLength =  Math.random() > 0.8 ? 1 : 7 // seconds
    synthLength =  Math.random() > 0.9 ? 2 : synthLength // seconds
    synthLength =  Math.random() > 0.95 ? 14 : synthLength // seconds

    // calculate position overlap
    let l = prevLength || synthLength
    let overlap = l / 3
    if (position > 0) {
      position -= overlap
    }

    // get synths from settings
    s.settings.pulseWaveGroups.forEach(group => {
      const numSynths = group.getNumSynths() 
      const panGroup = getPanArray(numSynths)
      for (let j = 0; j < numSynths; j++) { 
        let note = getRandomNote(scales[scaleIndex]) 
        playback.push({ 
          freq: scale[note] * group.pitchMul, 
          position: position + group.delay, 
          length: synthLength,
          pan: panGroup[j],
          numSynths: numSynths,
        }) 
      } 
    }) 
    // update position
    position += synthLength
    // set prev length
    prevLength = synthLength
  } 

  s.scheduler.on('data', function(schedule) { 
    // schedule: from, to, time, beatDuration 
    playback.forEach(function(note){ 
      if (note.position >= schedule.from && note.position < schedule.to){ 
        var delta = note.position - schedule.from 
        var time = schedule.time + delta 
        var duration = note.length * schedule.beatDuration 
        cb(s, time, duration, note, out) 
      } 
    }) 
  }) 
 
  s.scheduler.setTempo(60) 
 
  setTimeout(function(){ 
    s.scheduler.start() 
  }, 500) 
}

const play = (s, at, length, note, out) => {

  let pulseWave = new PulseWave(s.audioCtx)
  pulseWave.osc.frequency.setValueAtTime(note.freq, at)
  let gain = s.audioCtx.createGain()
  gain.gain.value = 0
  const panner = s.audioCtx.createStereoPanner()
  panner.pan.setValueAtTime(note.pan, at)

  pulseWave.osc.connect(gain)
  gain.connect(panner)
  panner.connect(out)

  pulseWave.osc.onended = function(event) {
    pulseWave.osc.disconnect(gain)
    gain.disconnect(panner)
    panner.disconnect(out)
  }

  pulseWave.osc.start(at)

  // do warbles
  pulseWaveVariations(pulseWave, at, note.freq)

  // do envelope this way -- Enveolope module was causing noise on pixel and iphone
  gain.gain.setValueAtTime(0, at)
  gain.gain.linearRampToValueAtTime(0.0001, at + length / 4)
  gain.gain.linearRampToValueAtTime(1, at + length / 2)
  gain.gain.linearRampToValueAtTime(0.0000001, at + length)
  // this.input.gain.linearRampToValueAtTime(this.sustainVol, this.time + this.sustain)
  // this.input.gain.linearRampToValueAtTime(this.min, this.time + this.decay)
  pulseWave.osc.stop(at + length)
}

export const startPulseWave = () => {
  return (dispatch, getState) => {
    const s = getState()

     const pulseGain = s.audioCtx.createGain() 
    pulseGain.gain.value = 1
    pulseGain.connect(s.masterGain) 
 
    // add to state 
    const synthOptions = { 
      id: 'synth', 
      gain: pulseGain, 
      gainValue: 100
    } 
 
    dispatch(setSynthNode(synthOptions)) 


    const compressor = s.audioCtx.createDynamicsCompressor();
    compressor.threshold.setValueAtTime(-80, s.audioCtx.currentTime);
    compressor.knee.setValueAtTime(40, s.audioCtx.currentTime);
    compressor.ratio.setValueAtTime(15, s.audioCtx.currentTime);
    compressor.attack.setValueAtTime(0, s.audioCtx.currentTime);
    compressor.release.setValueAtTime(0.25, s.audioCtx.currentTime);


    const lowpass = new TripleLowpass(s.audioCtx)
    const reverb = new Reverb(s.audioCtx) 
    reverb.time = 5 
    reverb.wet.value = 1
    reverb.dry.value = 0.45 
    reverb.filterType = 'lowpass' 
    reverb.cutoff.value = 4000 
 
    // pulseGain.connect(compressor)
    compressor.connect(lowpass.input)
    lowpass.connect(reverb) 
    reverb.connect(pulseGain) 
    pulseGain.connect(s.masterGain)

    startScheduler(s, compressor, play) 
  }
}