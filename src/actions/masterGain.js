export const setMasterGain = payload => ({
  type: 'SET_MASTER_GAIN',
  payload: payload
})

export const setMasterGainValue = payload => ({
  type: 'SET_MASTER_GAIN_VALUE',
  payload: payload
})

export const updateMasterGain = value => {
  return (dispatch, getState) => {
    const s = getState()
    // update synth
    const gainValue = value * 0.01
    s.masterGain.gain.setTargetAtTime(
      gainValue, 
      s.audioCtx.currentTime + 0.1, 
      0.01
    )
    // update ui state
    dispatch(setMasterGainValue(value))
  }
}