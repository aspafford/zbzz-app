export const pulseWaveVariations = (pad, at, freq) => {
  // max/min
  const variations = Math.ceil(Math.random() * 3) // variations per length
  const interval = 1 / variations
  let freqOffsetMax = freq * (Math.random() * 0.015) // max 2% frequency warble

  let i = 0
  while (i < variations) {

    let rand = Math.random()

    let pwOffset = rand * 0.8 + 0.1 // 0.1-0.9
    let freqOffset = rand > 0.5 ? rand * freqOffsetMax * -1 : rand * freqOffsetMax // -3 - +3

    let pwTimeOffset = Math.random() * interval
    let freqTimeOffset = Math.random() * interval

    // pulse width
    pad.widthGain.gain.linearRampToValueAtTime(pwOffset, at + i + pwTimeOffset)
    // frequency
    pad.osc.frequency.linearRampToValueAtTime(freq + freqOffset, at + i + freqTimeOffset)

    i += interval 
  }
}