import React, { Component } from 'react';
import { applyMiddleware, compose, createStore } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
// reducers
import reducer from './reducers/index.js'
// actions
import { initSynth } from './actions/initSynth'
import { loadSettings } from './actions/loadSettings'
// components
import { PlayButton } from './components/PlayButton'
import { Scope } from './components/Scope'
import MasterVolume from './components/MasterVolume'
import SynthVolume from './components/SynthVolume'

// synth settings
import settings from './synthSettings.js'

import './App.css';

// enable panning for safari
import StereoPannerNode from 'stereo-panner-node'
StereoPannerNode.polyfill()

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, /* preloadedState, */ composeEnhancers(
  applyMiddleware(thunk, logger)
));

class App extends Component {

  componentDidMount() {
    store.dispatch(loadSettings(settings))
    store.dispatch(initSynth())
  }

  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <MasterVolume/>
          <SynthVolume/>
          <PlayButton/>
          <Scope/>
        </div>
      </Provider>
    );
  }
}

export default App;
