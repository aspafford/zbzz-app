const pulseCurve =new Float32Array(256);
for (var i=0; i<128; i++){
  pulseCurve[i]=-1;
  pulseCurve[i+128]=1;
}

const constantCurve = new Float32Array(2);
constantCurve[0] = 0.6;
constantCurve[1] = 0.6;

function PulseWave(context, out, options) {

  this.osc = context.createOscillator();
  this.osc.type = 'sawtooth';
  
  //Shape the output into a pulse wave.
  const pulseShaper=context.createWaveShaper();
  pulseShaper.curve=pulseCurve;
  //Use a GainNode as our new "width" audio parameter.
  this.widthGain=context.createGain();
  this.widthGain.gain.value=0; //Default width.
  this.osc.width=this.widthGain.gain; //Add parameter to oscillator node.
  //Pass a constant value of 1 into the widthGain – so the "width" setting
  //is duplicated to its output.
  this.constantShaper = context.createWaveShaper();
  this.constantShaper.curve = constantCurve;

  this.osc.connect(pulseShaper);
  this.osc.connect(this.constantShaper);
  this.widthGain.connect(pulseShaper);
  this.constantShaper.connect(this.widthGain);

  //Override the oscillator's "connect" and "disconnect" method so that the
  //new node's output actually comes from the pulseShaper.
  this.osc.connect=function() {
    pulseShaper.connect.apply(pulseShaper, arguments);
  }
  this.osc.disconnect=function() {
    pulseShaper.disconnect.apply(pulseShaper, arguments);
  }
}

PulseWave.prototype.start = function(time, options) {
  this.osc.start(time);
}

PulseWave.prototype.stop = function(time) {
  this.osc.stop(time)
}

module.exports = PulseWave
