function Sine(context) {
  this.context = context
  this.osc = this.context.createOscillator()
  this.osc.type = "sine"
}

Sine.prototype.start = function(time) {
  this.osc.start(time);
}

Sine.prototype.stop = function(time) {
  this.osc.stop(time)
}

module.exports = Sine