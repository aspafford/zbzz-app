export class Envelope {
  constructor(audioCtx, at, options) {
    this.input = audioCtx.createGain()
    this.output = audioCtx.createGain()

    this.input.gain.value = 0

    this.time = at

    this.attack = options.length / 3
    this.sustain = this.attack + options.length / 3
    this.decay = this.sustain + options.length / 3

    this.attackVol = options.attackVol
    this.sustainVol = options.sustainVol

    this.min = 0.0000001
  }

  connect(target){

    this.input.gain.linearRampToValueAtTime(this.min, this.time)

    this.input.gain.linearRampToValueAtTime(this.attackVol, this.time + this.attack)
    this.input.gain.linearRampToValueAtTime(this.sustainVol, this.time + this.sustain)
    this.input.gain.linearRampToValueAtTime(this.min, this.time + this.decay)

    this.input.connect(this.output)

    this.output.connect(target)
  }

  disconnect(target){
    this.output.disconnect(target)
  }
}