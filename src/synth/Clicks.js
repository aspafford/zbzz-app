function Clicks(context, out) {

  // randomize amplitude of the click
  const randomAmplitude = Math.random()

  const numChannels = 2
  const bufferLength = 2
  const buffer = context.createBuffer(numChannels, bufferLength, context.sampleRate)
  // function to fill buffer
  for (let channel = 0; channel < numChannels; channel++) {
    // This gives us the actual ArrayBuffer that contains the data
    let nowBuffering = buffer.getChannelData(channel);
    nowBuffering[0] = randomAmplitude;
    nowBuffering[1] = 0
  }

  this.source = context.createBufferSource();
  this.source.buffer = buffer

  this.source.connect(out)
}

Clicks.prototype.start = function(time, options) {
  // randomly start sound within time range
  const offset = Math.random() * 1 / options.density
  this.source.start(time + offset)
}

Clicks.prototype.stop = function(time) {
  this.osc.stop(time)
}

module.exports = Clicks