function PinkNoise(context, out) {
  this.context = context

  // https://github.com/mohayonao/pink-noise-node
  // http://noisehack.com/generate-noise-web-audio-api/

  const noiseData = new Float32Array(44100 * 5);

  let b0 = 0, b1 = 0, b2 = 0, b3 = 0, b4 = 0, b5 = 0, b6 = 0;

  for (let i = 0, imax = noiseData.length; i < imax; i++) {
    let white = Math.random() * 2 - 1;

    b0 = 0.99886 * b0 + white * 0.0555179;
    b1 = 0.99332 * b1 + white * 0.0750759;
    b2 = 0.96900 * b2 + white * 0.1538520;
    b3 = 0.86650 * b3 + white * 0.3104856;
    b4 = 0.55000 * b4 + white * 0.5329522;
    b5 = -0.7616 * b5 - white * 0.0168980;

    noiseData[i] = b0 + b1 + b2 + b3 + b4 + b5 + b6 + white * 0.5362;
    noiseData[i] *= 0.11;
    b6 = white * 0.115926;
  }

  const noiseBuffer = this.context.createBuffer(1, noiseData.length, this.context.sampleRate); 
  noiseBuffer.getChannelData(0).set(noiseData); 
  const bufferSource = this.context.createBufferSource();

  bufferSource.buffer = noiseBuffer;
  bufferSource.loop = true;

  this.buffer = bufferSource

  // buffer passes through amplitude modulator
  this.gain = this.context.createGain()

  this.modGain = this.context.createGain()
  this.mod = this.context.createOscillator()
  this.buffer.connect(this.modGain)
  this.modGain.connect(this.gain)

  this.mod.connect(this.modGain.gain) /// WARNING: FIREFOX BUG mod lfo connect needs to happen AFTER audioCtx.resume()

  // also create a 'flat' buffer to fill in am volume dip
  const bufferSource2 = this.context.createBufferSource();

  bufferSource2.buffer = noiseBuffer;
  bufferSource2.loop = true;

  this.buffer2 = bufferSource2
  this.gain2 = this.context.createGain()
  this.buffer2.connect(this.gain2)

  // bandpass filter
  this.filter = this.context.createBiquadFilter()
  this.filter.type = 'bandpass'
  // stereo panning
  this.pan = this.context.createStereoPanner()

  // connect both buffers to filter and pan
  this.gain.connect(this.filter)
  this.gain2.connect(this.filter)
  this.filter.connect(this.pan)
  this.pan.connect(out)
}

PinkNoise.prototype.start = function(time, options) {

  // set panning
  this.pan.pan.value = options.pan

  // set filter fq and width
  this.filter.frequency.setTargetAtTime(options.filterFq, time, 0.0)
  this.filter.Q.setTargetAtTime(options.filterQ, time, 0.0)

  // start noise buffer
  this.buffer.start(time);
  // start am
  this.mod.start(time)
  // am speed
  this.mod.frequency.setTargetAtTime(options.modFq, time, 0.0) 
  // am gain
  this.gain.gain.setTargetAtTime(options.maxGain, time, 0.0)

  // start second flat copy
  this.buffer2.start(time)
  // gain for flat buffer
  this.gain2.gain.setTargetAtTime(options.flatGain, time, 0.0)
}

PinkNoise.prototype.stop = function(time) {
  this.buffer.stop(time)
}

module.exports = PinkNoise
