class TripleLowpass {
  constructor(audioCtx) {
    this.input = audioCtx.createGain()
    this.output = audioCtx.createGain()

    this.f1 = audioCtx.createBiquadFilter()
    this.f2 = audioCtx.createBiquadFilter()
    this.f3 = audioCtx.createBiquadFilter()

    this.f1.type = "lowpass"
    this.f2.type = "lowpass"
    this.f3.type = "lowpass"
    this.f1.frequency.value = 1500
    this.f2.frequency.value = 1500
    this.f3.frequency.value = 1500

    this.input.connect(this.f1)

    this.f1.connect(this.f2)
    this.f2.connect(this.f3)
    this.f3.connect(this.output)
  }

  connect(target) {
    this.output.connect(target)
  }
  disconnect(target) {
    this.output.disconnect(target)
  }
}

module.exports = TripleLowpass