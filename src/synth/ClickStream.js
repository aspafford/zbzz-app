function ClickStream(context, out, options) {

  const bufferLength = options.bufferLength // seconds
  const numChannels = 2
  const numSamples = context.sampleRate * bufferLength

  // options
  const numClicks = options.numClicks * bufferLength
  const maxGain = options.gain

  const buffer = context.createBuffer(
    numChannels, 
    numSamples, 
    context.sampleRate
  ); 

  const randomSamplesArr = new Array(numClicks)
  for (let i = 0; i < numClicks; i++) {
    let range = numSamples / numClicks
    let offset = i * range
    let val = Math.floor(Math.random() * range + offset)
    randomSamplesArr[i] = val
  }

  let randSampleIndex = 0
  const nowBufferingL = buffer.getChannelData(0);
  const nowBufferingR = buffer.getChannelData(1);
  for (let sample = 0; sample < numSamples; sample++) {
    if (sample === randomSamplesArr[randSampleIndex]) {
      if (randSampleIndex < numClicks) randSampleIndex += 1
      nowBufferingL[sample] = Math.random() * maxGain
      nowBufferingR[sample] = Math.random() * maxGain
    } else {
      nowBufferingL[sample] = 0
      nowBufferingR[sample] = 0
    }
  }

  this.source = context.createBufferSource();
  this.source.buffer = buffer
  this.source.loop = true

  this.source.connect(out)
}

ClickStream.prototype.start = function(time, options) {
  this.source.start(time);
}

ClickStream.prototype.stop = function(time) {
  this.buffer.stop(time)
}

module.exports = ClickStream
