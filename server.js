var express = require('express')

var app = express()

var port = 8080

app.use('/', express.static(__dirname + '/build'));

app.listen(port, function() {
  console.log("Node app is running at localhost:" + port)
})
